CHANGELOG
=========

1.1.1 (2015-11-18)
------------------

* fixed bug in request headers

1.1.0 (2015-11-18)
------------------

* rewrote backend and tests to support Guzzle v6

1.0.1 (2015-03-22)
------------------

* removed redundant closing php tag from all files

1.0.0 (2015-03-17)
------------------

* bump version to 1.0.0

0.1.0 (2015-03-17)
------------------

* initial release
