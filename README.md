HipChat Notifier
================

A HipChat API wrapper using Guzzle v6 which implements a simple room notification interface using the HipChat v2 API and
is designed to be used with room authorization tokens.

By [Simon Hampel](http://hampelgroup.com/).

Installation
------------

The recommended way of installing HipChat Notifier is through [Composer](http://getcomposer.org):

    :::json
    {
        "require": {
            "hampel/hipchat-notify": "~1.0"
        }
    }

Configuration
-------------

To use the HipChat Notifier, you'll need to generate a room authorization token using the HipChat Group admin area.

Log in to HipChat and then go to `Group admin`.

Next, go to the `Rooms` area and find (or create) a room where notifications will be sent. Click on that room in the list
to edit the settings.

On the right of the room details will be a `Tokens` link - click on this to see a list of any existing tokens already
defined for this room.

Create a new token by entering a Label and then clicking `Create`.

Note that the label will be what is displayed as the user sending the message when you use the notifier, so choose
something meaningful. You can always create multiple tokens for different notification sources if required.

Once you have a token, make a note of the token string - you'll need to use this whenever we ask for
`<room_access_token>`.

You'll also need to make a note of the room API ID on the summary page, although you can retrieve this directly using
the API if you wish - however it might be better to store this in a configuration setting to avoid unnecessary network
calls to the API.

Usage
-----

### Messages ###

HipChat messages consist of four elements - the content of the message, an associated background colour displayed in
the HipChat client, a boolean toggle indicating whether to notify the user about the message or not, and an indicator
as to the format of the message content (text vs html).

To create messages, use the `HipChat\Message` class:

    :::php
    <?php
    	use HipChat\Message;
    
    	$message = new Message('message to be posted', Message::COLOR_GREEN, true, Message::FORMAT_TEXT);
    	var_dump($message->toJson());
    ?>

Messages created using this class are immutable - once created, they cannot be changed without creating a new object.

For simple messages, two static helpers have been created which use the default settings (yellow background colour, 
don't notify).
 
    :::php
    <?php
    	use HipChat\Message;
    
    	$message = Message::createHtml('<strong>html</strong> message to be posted');
    	var_dump($message->toJson());

    	$message = Message::createText('text message to be posted - can use @mentions');
    	var_dump($message->toJson());

    ?>

The JSON output of the `toJson()` call on a message will be in the format suitable for sending to the HipChat API.
 
### Notifier ###

The notifier does all the work of communicating with the API via Guzzle.

To use the notifier, you first need to create a Guzzle Client and pass the Notifier configuration to it, which sets the
base URL of the Client. Use the `Notifier::getConfig()` to retrieve the config array.


    :::php
    <?php
    	use GuzzleHttp\Client;
    	use HipChat\Message;
    	use HipChat\Notifier;
    
    	$client = new Client(Notifier::getConfig());
    
    	// use the room access token created in the HipChat admin area
		$hipchat = new Notifier($client, '<room_access_token>');
    
		$message = new Message('message to be posted', Message::COLOR_GREEN, true, Message::FORMAT_TEXT);
    
    	// send our message to the room with this ID
		$hipchat->send($message, '123456');
    ?>
    
Some shortcuts and tricks:

    :::php
    // static factory for creating a Notifier with client automatically
    $hipchat = Notifier::make('<room_access_token>');
    
    // get the room ID that this room access token is allowed to post to via the API
    $roomid = $hipchat->getRoomId();
    
    // get the full session details and use that to get the room id and room name with only a single API call
    $session = $hipchat->getSession();
    $roomid = $hipchat->getRoomId($session);
    $roomname = $hipchat->getRoomName($session)
    
Exception handling:

    :::php
    try
    {
    	$hipchat->send($message, '123456');
    }
    catch (HipChatException $e)
    {
    	echo $e->getMessage();
    	$response = $e->getResponse();
    	if (!is_null($response))
    	{
    		// examine the response in more detail
    	}
    }
