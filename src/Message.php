<?php  namespace HipChat; 

use HipChat\Exception\LengthException;
use HipChat\Exception\InvalidArgumentException;
use HipChat\Exception\UnexpectedValueException;

class Message
{
	const COLOR_YELLOW = 'yellow';
	const COLOR_GREEN = 'green';
	const COLOR_RED = 'red';
	const COLOR_PURPLE = 'purple';
	const COLOR_GRAY = 'gray';
	const COLOR_RANDOM = 'random';

	public static $colors = ['yellow', 'green', 'red', 'purple', 'gray', 'random'];

	const FORMAT_HTML = 'html';
	const FORMAT_TEXT = 'text';

	public static $formats = ['html', 'text'];

	public static $max_length = 10000;

	private $message;

	private $color;

	private $notify;

	private $message_format;

	public function __construct($message, $color = Message::COLOR_YELLOW, $notify = false, $message_format = Message::FORMAT_HTML)
	{
		if (empty($message))
		{
			throw new LengthException("Message must not be empty");
		}

		if (strlen($message) > self::$max_length)
		{
			throw new LengthException("Message must be less than " . number_format(self::$max_length) . " characters long");
		}

		if (!in_array($color, self::$colors))
		{
			throw new UnexpectedValueException("Color [{$color}] not allowed, use one of: " . implode(", ", self::$colors));
		}

		if (!is_bool($notify))
		{
			throw new InvalidArgumentException("Notify parameter must be boolean");
		}

		if (!in_array($message_format, self::$formats))
		{
			throw new UnexpectedValueException("Message format [{$message_format}] not allowed, use one of: " . implode(", ", self::$formats));
		}

		$this->message = $message;
		$this->color = $color;
		$this->notify = $notify;
		$this->message_format = $message_format;
	}

	public static function createHtml($message)
	{
		return new static($message, Message::COLOR_YELLOW, false, Message::FORMAT_HTML);
	}

	public static function createText($message)
	{
		return new static($message, Message::COLOR_YELLOW, false, Message::FORMAT_TEXT);
	}

	public function getMessage()
	{
		return $this->message;
	}

	public function getColor()
	{
		return $this->color;
	}

	public function getNotify()
	{
		return $this->notify;
	}

	public function getMessageFormat()
	{
		return $this->message_format;
	}

	public function toJson()
	{
		return json_encode([
			'message' => $this->message,
			'color' => $this->color,
			'notify' => $this->notify,
			'message_format' => $this->message_format
		]);
	}

	public function equals(Message $other)
	{
		return
			($this->message === $other->message)
			&& ($this->color === $other->color)
			&& ($this->notify === $other->notify)
			&& ($this->message_format === $other->message_format);
	}
}
