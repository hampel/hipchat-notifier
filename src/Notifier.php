<?php namespace HipChat;

use Hampel\Json\Json;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Hampel\Json\JsonException;
use GuzzleHttp\ClientInterface;
use HipChat\Exception\RuntimeException;
use GuzzleHttp\Exception\ParseException;
use GuzzleHttp\Exception\RequestException;
use HipChat\Exception\HipChatParseException;
use HipChat\Exception\HipChatRequestException;

/**
 * The main service interface using Guzzle
 */
class Notifier
{
	/** @var string base url for API calls */
	protected static $base_url = 'https://api.hipchat.com/v2/';

	/** @var string access token */
	protected $token;

	/** @var Client our Guzzle HTTP Client object */
	protected $client;

	/** @var Request Psr7 Request object representing the last request made */
	protected $last_request;

	/** @var Response Psr7 Response object representing the last response from Guzzle call to HipChat API */
	protected $last_response;

	/** @var  string a string description of the last action taken */
	protected $last_action;

	/**
	 * Constructor
	 *
	 * @param Client $client	Guzzle HTTP client
	 * @param string $token		HipChat access token
	 */
	public function __construct(ClientInterface $client, $token = "")
	{
		$this->client = $client;
		$this->token = $token;
	}

	/**
	 * Make - construct a service object
	 *
	 * @param string $token
	 *
	 * @return Notifier a fully hydrated HipChat Service, ready to run
	 */
	public static function make($token)
	{
		return new self(new Client(self::getConfig()), $token);
	}

	public static function getConfig()
	{
		return ['base_uri' => self::$base_url];
	}

	public function getClient()
	{
		return $this->client;
	}

	public function getRoomId($session = null)
	{
		$session = $session ?: $this->getSession();

		return $session['client']['room']['id'];
	}

	public function getRoomName($session = null)
	{
		$session = $session ?: $this->getSession();

		return $session['client']['room']['name'];
	}

	public function getSession()
	{
		return $this->get("oauth/token/{$this->token}");
	}

	public function send(Message $message, $room_id = null)
	{
		if (is_null($room_id))
		{
			$room_id = $this->getRoomId();
		}

		return $this->post("room/{$room_id}/notification", $message->toJson());
	}

	public function get($action)
	{
		$this->last_action = "GET {$action}";

		return $this->process($action, "GET");
	}

	public function post($action, $data = null)
	{
		$this->last_action = "POST {$action}";

		return $this->process($action, "POST", $data);
	}

	protected function process($action, $method = "GET", $data = null)
	{
		if (empty($this->token))
		{
			throw new RuntimeException("Access token not yet set");
		}

		$headers = [
			'Authorization' => "Bearer {$this->token}",
			'content-type' => 'application/json',
		];

		$request = new Request($method, $action, $headers, $data);

		$this->last_request = $request;

		try
		{
			$response = $this->client->send($request);
		}
		catch (RequestException $e)
		{
			throw new HipChatRequestException(
				"HipChat " . $e->getMessage(),
				$e->getCode(),
				$e
			);
		}

		$this->last_response = $response;

		$body = $response->getBody();

		if ($body->getSize() > 0)
		{
			try
			{
				return Json::decode($body->getContents(), Json::DECODE_ASSOC);
			}
			catch (JsonException $e)
			{
				throw new HipChatParseException(
					"HipChat " . $e->getMessage() . " - last command [{$this->last_action}]",
					$e->getCode(),
					$e
				);
			}
		}
	}

	/**
	 * Return the request object from the last API call made
	 *
	 * @return Request Psr7 Request object
	 */
	public function getLastRequest()
	{
		return $this->last_request;
	}

	/**
	 * Return the response object from the last API call made
	 *
	 * @return Response Guzzle Reponse object
	 */
	public function getLastResponse()
	{
		return $this->last_response;
	}

	/**
	 * Return the status code from the last API call made
	 *
	 * @return number status code
	 */
	public function getLastStatusCode()
	{
		$last_response = $this->getLastResponse();
		if (!is_null($last_response))
		{
			return $last_response->getStatusCode();
		}
	}

	public function getLastQuery()
	{
		$last_request = $this->getLastRequest();
		if (!is_null($last_request))
		{
//			return strval(\GuzzleHttp\Psr7\Uri::resolve(\GuzzleHttp\Psr7\uri_for($this->client->getConfig('base_uri')), $last_request->getUri()));
			return strval($last_request->getUri());
		}
	}

	public function getLastAction()
	{
		return $this->last_action;
	}

	public function setToken($token)
	{
		$this->token = $token;
	}
}
