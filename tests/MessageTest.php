<?php namespace HipChat;

class MessageTest extends \PHPUnit_Framework_TestCase
{
	public function testBadMessage1()
	{
		$this->setExpectedException('HipChat\Exception\LengthException', 'Message must not be empty');

		new Message('');
	}

	public function testBadMessage2()
	{
		$this->setExpectedException('HipChat\Exception\LengthException', 'Message must be less than 10,000');

		new Message(str_repeat('-', 10001));
	}

	public function testBadMessage3()
	{
		$this->setExpectedException('HipChat\Exception\UnexpectedValueException', 'Color [] not allowed, use one of: yellow, green, red, purple, gray, random');

		new Message('foo', '');
	}

	public function testBadMessage4()
	{
		$this->setExpectedException('HipChat\Exception\InvalidArgumentException', 'Notify parameter must be boolean');

		new Message('foo', Message::COLOR_YELLOW, 'bar');
	}

	public function testBadMessage5()
	{
		$this->setExpectedException('HipChat\Exception\UnexpectedValueException', 'Message format [json] not allowed, use one of: html, text');

		new Message('foo', Message::COLOR_YELLOW, false, 'json');
	}

	public function testMessage()
	{
		$message = new Message('foo');

		$this->assertEquals('foo', $message->getMessage());
		$this->assertEquals('yellow', $message->getColor());
		$this->assertFalse($message->getNotify());
		$this->assertEquals('html', $message->getMessageFormat());

		$json = json_encode(['message' => 'foo', 'color' => 'yellow', 'notify' => false, 'message_format' => 'html']);

		$this->assertEquals($json, $message->toJson());

		$message2 = new Message('foo', 'yellow', false, 'html');
		$this->assertTrue($message->equals($message2));

		$message3 = Message::createHtml('foo');
		$this->assertEquals('foo', $message3->getMessage());
		$this->assertEquals('yellow', $message3->getColor());
		$this->assertFalse($message3->getNotify());
		$this->assertEquals('html', $message3->getMessageFormat());
		$this->assertTrue($message->equals($message3));

		$message4 = Message::createText('foo');
		$this->assertEquals('foo', $message4->getMessage());
		$this->assertEquals('yellow', $message4->getColor());
		$this->assertFalse($message4->getNotify());
		$this->assertEquals('text', $message4->getMessageFormat());
		$this->assertFalse($message->equals($message4));
	}
}
