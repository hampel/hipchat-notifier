<?php namespace HipChat;

use Mockery;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Exception\RequestException;

class NotifierTest extends \PHPUnit_Framework_TestCase
{
	public function setUp()
	{
		$this->mock = new MockHandler();

		$this->client = new Client(['base_uri' => 'http://example.com', 'handler' => $this->mock]);

		$this->message = Mockery::mock('HipChat\Message');
	}

	protected function loadMockResponse($filename)
	{
		return \GuzzleHttp\Psr7\parse_response($this->loadMockData($filename));
	}

	protected function loadMockData($filename)
	{
		return file_get_contents($this->getMockPath() . $filename);
	}

	protected function getMockPath()
	{
		return dirname(__FILE__) . DIRECTORY_SEPARATOR . "mock" . DIRECTORY_SEPARATOR;
	}

	public function testGetConfig()
	{
		$config = Notifier::getConfig();

		$this->assertArrayHasKey('base_uri', $config);
		$this->assertEquals('https://api.hipchat.com/v2/', $config['base_uri']);
	}

	public function testMake()
	{
		$hipchat = Notifier::make('foo');

		/** @var GuzzleHttp\Client $client */
		$client = $hipchat->getClient();

		$config = $client->getConfig();

		$this->assertTrue(is_array($config));
		$this->assertArrayHasKey('base_uri', $config);
		$this->assertEquals('https://api.hipchat.com/v2/', $config['base_uri']);
	}

	public function testNoTokenException()
	{
		$this->setExpectedException('HipChat\Exception\RuntimeException', 'Access token not yet set');

		(new Notifier($this->client))->get('foo');
	}

	public function testRequestException()
	{
		$this->mock->append(new RequestException('foo', new Request('GET', 'http://example.com')));

		$this->setExpectedException('HipChat\Exception\HipChatRequestException', 'HipChat foo');

		(new Notifier($this->client, 'bar'))->get('baz');
	}

	public function testNoContent()
	{
		$this->mock->append(new Response(204));

        $hipchat = new Notifier($this->client, 'bar');
        $data = $hipchat->get('baz');
        $response = $hipchat->getLastResponse();

        $this->assertNull($data);
        $this->assertEquals(204, $response->getStatusCode());
        $this->assertEquals('No Content', $response->getReasonPhrase());
        $this->assertEquals('baz', $hipchat->getLastQuery());

        $this->assertNull($data);
        $this->assertEquals(204, $response->getStatusCode());
        $this->assertEquals('No Content', $response->getReasonPhrase());
        $this->assertEquals('baz', $hipchat->getLastQuery());

        $this->assertEquals(204, $hipchat->getLastStatusCode());
        $this->assertEquals('baz', $hipchat->getLastQuery());
        $this->assertEquals('GET baz', $hipchat->getLastAction());

	}

	public function testInvalidJson()
	{
		$this->mock->append($this->loadMockResponse('invalid.json'));

		$this->setExpectedException('HipChat\Exception\HipChatParseException', 'HipChat Error decoding JSON: Syntax error - last command [GET baz]');

		(new Notifier($this->client, 'bar'))->get('baz');
	}

	public function testGetSession()
	{
		$this->mock->append($this->loadMockResponse('session.json'));

		$hipchat = new Notifier($this->client, 'foobarbaz');

		$session = $hipchat->getSession();

		$this->assertArrayHasKey('access_token', $session);
		$this->assertEquals('foobarbaz', $session['access_token']);
		$this->assertArrayHasKey('client', $session);
		$this->assertArrayHasKey('room', $session['client']);
		$this->assertArrayHasKey('id', $session['client']['room']);
		$this->assertEquals('123456', $session['client']['room']['id']);
		$this->assertArrayHasKey('name', $session['client']['room']);
		$this->assertEquals('Foo', $session['client']['room']['name']);

		$this->assertEquals('123456', $hipchat->getRoomId($session));
		$this->assertEquals('Foo', $hipchat->getRoomName($session));
	}

	public function testSend()
	{
		$this->message->shouldReceive('toJson')->andReturn(file_get_contents($this->getMockPath() . 'message.json'));
		$this->mock->append($this->loadMockResponse('204.json'));

		$hipchat = new Notifier($this->client, 'foobarbaz');
		$hipchat->send($this->message, '123456');

		$this->assertEquals(204, $hipchat->getLastStatusCode());
		$this->assertEquals('room/123456/notification', $hipchat->getLastQuery());
		$this->assertEquals('POST room/123456/notification', $hipchat->getLastAction());

	}

    public function tearDown()
    {
        Mockery::close();
    }
}
